resource "google_compute_resource_policy" "os_disk_snapshot_policy" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project

  name = format(
    "%v-os-snapshot-policy",
    var.name
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.hours_between_os_disk_snapshots
        start_time     = "00:00"
      }
    }

    snapshot_properties {
      guest_flush = var.enable_os_snapshot_guest_scripts
      labels      = var.labels
    }

    retention_policy {
      max_retention_days    = var.os_disk_snapshot_max_retention_days
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }
  }
}

locals {
  default_instances_exist = length(google_compute_instance.default) > 0
  disk_name               = local.default_instances_exist ? google_compute_instance.default[0].name : google_compute_instance.instance_zoned[element(local.merged_nodes_list, 0)].name
  disk_zone               = local.default_instances_exist ? google_compute_instance.default[0].zone : google_compute_instance.instance_zoned[element(local.merged_nodes_list, 0)].zone
}

resource "google_compute_disk_resource_policy_attachment" "instance_with_multi_disks_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = local.disk_name
  zone    = local.disk_zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}
