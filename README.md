# GitLab.com Generic Service with Group Terraform Module

## What is this?

This module provisions generic service GCE instance groups.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 6.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 6.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bootstrap"></a> [bootstrap](#module\_bootstrap) | ops.gitlab.net/gitlab-com/bootstrap/google | 5.5.8 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.external](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address_zoned](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_backend_service) | resource |
| [google_compute_backend_service.iap](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_backend_service) | resource |
| [google_compute_disk.log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.log_disk_zoned](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.os_disk_from_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.os_disk_from_snapshot_zoned](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_multi_disks_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_firewall.deny_all_egress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.public](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_network](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_world](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_health_check.http](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_health_check.tcp](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_instance.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.instance_zoned](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance_group.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_group) | resource |
| [google_compute_region_backend_service.additional_nics](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_region_backend_service) | resource |
| [google_compute_region_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_region_backend_service) | resource |
| [google_compute_resource_policy.os_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_subnetwork.additional_subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_subnetwork) | data source |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_nics"></a> [additional\_nics](#input\_additional\_nics) | Example value is as follows: [{ "network\_name"  = "foo-vpc" "ip\_cidr\_range" = "10.10.0.0/20" }] | `list(map(string))` | `[]` | no |
| <a name="input_additional_scopes"></a> [additional\_scopes](#input\_additional\_scopes) | Additional permission scopes. | `list(string)` | `[]` | no |
| <a name="input_additional_tags"></a> [additional\_tags](#input\_additional\_tags) | Additional instance tags | `list(string)` | `[]` | no |
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Whether Terraform is allowed to stop the instance to update its properties | `bool` | `false` | no |
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Instances without public IPs cannot access the public internet without NAT. Ensure that a Cloud NAT instance covers the subnetwork/region for this instance. | `bool` | `true` | no |
| <a name="input_backend_balancing_mode"></a> [backend\_balancing\_mode](#input\_backend\_balancing\_mode) | Balancing mode of the backend service: `UTILIZATION`, `RATE` or `CONNECTION` | `string` | `"UTILIZATION"` | no |
| <a name="input_backend_connection_draining_timeout"></a> [backend\_connection\_draining\_timeout](#input\_backend\_connection\_draining\_timeout) | Time (in seconds) for which instance will be drained from the backend service (not accept new connections, but still work to finish started) | `number` | `300` | no |
| <a name="input_backend_protocol"></a> [backend\_protocol](#input\_backend\_protocol) | n/a | `string` | `"HTTP"` | no |
| <a name="input_backend_service_type"></a> [backend\_service\_type](#input\_backend\_service\_type) | Type of backend service, either normal or regional | `string` | `"regular"` | no |
| <a name="input_balance_across_zones"></a> [balance\_across\_zones](#input\_balance\_across\_zones) | n/a | `bool` | `false` | no |
| <a name="input_block_project_ssh_keys"></a> [block\_project\_ssh\_keys](#input\_block\_project\_ssh\_keys) | Whether to block project level SSH keys | `bool` | `true` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | User-provided bootstrap script to override the bootstrap module | `string` | `null` | no |
| <a name="input_chef_init_run_list"></a> [chef\_init\_run\_list](#input\_chef\_init\_run\_list) | run\_list for the node in chef that are ran on the first boot only | `string` | `""` | no |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | `map(string)` | n/a | yes |
| <a name="input_chef_run_list"></a> [chef\_run\_list](#input\_chef\_run\_list) | run\_list for the node in chef | `string` | n/a | yes |
| <a name="input_create_backend_service"></a> [create\_backend\_service](#input\_create\_backend\_service) | n/a | `bool` | `true` | no |
| <a name="input_default_scopes"></a> [default\_scopes](#input\_default\_scopes) | default permission scopes. | `list(string)` | <pre>[<br/>  "https://www.googleapis.com/auth/cloud.useraccounts.readonly",<br/>  "https://www.googleapis.com/auth/devstorage.read_only",<br/>  "https://www.googleapis.com/auth/logging.write",<br/>  "https://www.googleapis.com/auth/monitoring.write",<br/>  "https://www.googleapis.com/auth/pubsub",<br/>  "https://www.googleapis.com/auth/service.management.readonly",<br/>  "https://www.googleapis.com/auth/servicecontrol",<br/>  "https://www.googleapis.com/auth/trace.append",<br/>  "https://www.googleapis.com/auth/cloudkms",<br/>  "https://www.googleapis.com/auth/compute.readonly"<br/>]</pre> | no |
| <a name="input_deletion_protection"></a> [deletion\_protection](#input\_deletion\_protection) | n/a | `bool` | `false` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_egress_ports"></a> [egress\_ports](#input\_egress\_ports) | The list of ports that should be opened for egress traffic | `list(string)` | `[]` | no |
| <a name="input_enable_iap"></a> [enable\_iap](#input\_enable\_iap) | n/a | `bool` | `false` | no |
| <a name="input_enable_os_disk_snapshots"></a> [enable\_os\_disk\_snapshots](#input\_enable\_os\_disk\_snapshots) | Enable creation of OS disk snapshot resource and attachment policies | `bool` | `false` | no |
| <a name="input_enable_os_snapshot_guest_scripts"></a> [enable\_os\_snapshot\_guest\_scripts](#input\_enable\_os\_snapshot\_guest\_scripts) | Enables application-consistent snapshots so that pre and post snap scripts are executed | `bool` | `true` | no |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Whether to enable OS Login GCP feature | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_health_check"></a> [health\_check](#input\_health\_check) | n/a | `string` | `"http"` | no |
| <a name="input_health_check_port"></a> [health\_check\_port](#input\_health\_check\_port) | Health check port, if 0 use `service_port` (default) | `number` | `0` | no |
| <a name="input_hours_between_os_disk_snapshots"></a> [hours\_between\_os\_disk\_snapshots](#input\_hours\_between\_os\_disk\_snapshots) | Setting this will enable hourly os disk snapshots, 0 to disable | `number` | `4` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | The IP range | `string` | `"169.254.0.1/32"` | no |
| <a name="input_kernel_version"></a> [kernel\_version](#input\_kernel\_version) | n/a | `string` | `""` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The machine size | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_node_count"></a> [node\_count](#input\_node\_count) | The nodes count | `number` | n/a | yes |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | n/a | <pre>map(object({<br/>    chef_run_list_extra    = optional(string, "")<br/>    zone                   = optional(string, "")<br/>    balance_across_zones   = optional(bool, false)<br/>    machine_type           = optional(string, "")<br/>    os_disk_type           = optional(string, "")<br/>    log_disk_type          = optional(string, "")<br/>    os_disk_snapshot       = optional(string, "")<br/>    os_boot_image_override = optional(string, "")<br/>    additional_labels      = optional(map(string), {})<br/>    labels_override        = optional(map(string), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_nodes_count"></a> [nodes\_count](#input\_nodes\_count) | Number of nodes to provision using the 'nodes' map in a for\_each loop. | `number` | `0` | no |
| <a name="input_nodes_offset"></a> [nodes\_offset](#input\_nodes\_offset) | The starting index for provisioning nodes with the `nodes` map or `nodes_count` variables. Useful when migrating from provisioning using count to for\_each | `number` | `0` | no |
| <a name="input_nodes_overrides"></a> [nodes\_overrides](#input\_nodes\_overrides) | Node parameters to override for a specific node index that is provisioned using `nodes` or `nodes_count`. | <pre>map(object({<br/>    chef_run_list_extra    = optional(string, "")<br/>    zone                   = optional(string, "")<br/>    balance_across_zones   = optional(bool, false)<br/>    machine_type           = optional(string, "")<br/>    os_disk_type           = optional(string, "")<br/>    log_disk_type          = optional(string, "")<br/>    os_disk_snapshot       = optional(string, "")<br/>    os_boot_image_override = optional(string, "")<br/>    additional_labels      = optional(map(string), {})<br/>    labels_override        = optional(map(string), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_oauth2_client_id"></a> [oauth2\_client\_id](#input\_oauth2\_client\_id) | n/a | `string` | `""` | no |
| <a name="input_oauth2_client_secret"></a> [oauth2\_client\_secret](#input\_oauth2\_client\_secret) | n/a | `string` | `""` | no |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-2004-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `20` | no |
| <a name="input_os_disk_snapshot_max_retention_days"></a> [os\_disk\_snapshot\_max\_retention\_days](#input\_os\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 3. | `number` | `3` | no |
| <a name="input_os_disk_snapshot_nodes"></a> [os\_disk\_snapshot\_nodes](#input\_os\_disk\_snapshot\_nodes) | Map of node index => snapshot self\_link to use when creating new nodes from an OS disk snapshot instead of existing boot image | `map(string)` | `{}` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-standard"` | no |
| <a name="input_preemptible"></a> [preemptible](#input\_preemptible) | Use preemptible instances for this pet | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_public_ports"></a> [public\_ports](#input\_public\_ports) | The list of ports that should be publicly reachable | `list(string)` | `[]` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | n/a | yes |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_service_path"></a> [service\_path](#input\_service\_path) | n/a | `string` | `"/"` | no |
| <a name="input_service_port"></a> [service\_port](#input\_service\_port) | n/a | `number` | `80` | no |
| <a name="input_static_private_ip"></a> [static\_private\_ip](#input\_static\_private\_ip) | Create Google compute instances with static internal IP addresses | `bool` | `false` | no |
| <a name="input_static_private_ip_auto_assign"></a> [static\_private\_ip\_auto\_assign](#input\_static\_private\_ip\_auto\_assign) | Let the static internal IP addresses be assigned automatically instead of assigning them explicitly | `bool` | `false` | no |
| <a name="input_subnetwork_name"></a> [subnetwork\_name](#input\_subnetwork\_name) | Subnetwork name for the instances | `string` | `""` | no |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | User-provided teardown script to override the bootstrap module | `string` | `null` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for this service | `string` | n/a | yes |
| <a name="input_timeout_sec"></a> [timeout\_sec](#input\_timeout\_sec) | n/a | `number` | `30` | no |
| <a name="input_use_external_ip"></a> [use\_external\_ip](#input\_use\_external\_ip) | n/a | `bool` | `false` | no |
| <a name="input_use_new_node_name"></a> [use\_new\_node\_name](#input\_use\_new\_node\_name) | n/a | `bool` | `false` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The target network | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | DEPRECATED, use zones instead. Create instances in the specified zone instead of all region zones | `string` | `""` | no |
| <a name="input_zones"></a> [zones](#input\_zones) | List of zones to create Instances in, defaults to all region zones if left empty | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_google_compute_additional_subnetworks_self_link"></a> [google\_compute\_additional\_subnetworks\_self\_link](#output\_google\_compute\_additional\_subnetworks\_self\_link) | n/a |
| <a name="output_google_compute_address_static_ips"></a> [google\_compute\_address\_static\_ips](#output\_google\_compute\_address\_static\_ips) | n/a |
| <a name="output_google_compute_backend_service_iap_self_link"></a> [google\_compute\_backend\_service\_iap\_self\_link](#output\_google\_compute\_backend\_service\_iap\_self\_link) | n/a |
| <a name="output_google_compute_backend_service_self_link"></a> [google\_compute\_backend\_service\_self\_link](#output\_google\_compute\_backend\_service\_self\_link) | This is idiomatic for how to have outputs that may have a count of zero https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0 |
| <a name="output_google_compute_region_backend_service_additional_nics_self_link"></a> [google\_compute\_region\_backend\_service\_additional\_nics\_self\_link](#output\_google\_compute\_region\_backend\_service\_additional\_nics\_self\_link) | n/a |
| <a name="output_google_compute_region_backend_service_self_link"></a> [google\_compute\_region\_backend\_service\_self\_link](#output\_google\_compute\_region\_backend\_service\_self\_link) | n/a |
| <a name="output_google_compute_subnetwork_name"></a> [google\_compute\_subnetwork\_name](#output\_google\_compute\_subnetwork\_name) | n/a |
| <a name="output_google_compute_subnetwork_self_link"></a> [google\_compute\_subnetwork\_self\_link](#output\_google\_compute\_subnetwork\_self\_link) | n/a |
| <a name="output_http_health_check_self_link"></a> [http\_health\_check\_self\_link](#output\_http\_health\_check\_self\_link) | n/a |
| <a name="output_instance_groups_self_link"></a> [instance\_groups\_self\_link](#output\_instance\_groups\_self\_link) | n/a |
| <a name="output_instances"></a> [instances](#output\_instances) | n/a |
| <a name="output_instances_self_link"></a> [instances\_self\_link](#output\_instances\_self\_link) | n/a |
| <a name="output_tcp_health_check_self_link"></a> [tcp\_health\_check\_self\_link](#output\_tcp\_health\_check\_self\_link) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
