# 
# Common resources for instance.tf and instance_multizone.tf
#
module "bootstrap" {
  source  = "ops.gitlab.net/gitlab-com/bootstrap/google"
  version = "5.5.8"
}

resource "google_compute_address" "external" {
  project = var.project
  count   = var.use_external_ip ? var.node_count + length(local.merged_nodes) : 0

  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  address_type = "EXTERNAL"
  region       = var.region
}

resource "google_compute_region_backend_service" "default" {
  project = var.project
  count   = var.backend_service_type == "regional" && var.create_backend_service ? 1 : 0

  name     = format("%v-%v-regional", var.environment, var.name)
  protocol = "TCP"

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[2].self_link
  }

  connection_draining_timeout_sec = var.backend_connection_draining_timeout

  health_checks = local.health_checks

  lifecycle {
    precondition {
      # When we set `create_backend_service` to true, we need to have instances divisible by 3.
      # If we do not set `create_backend_service` we don't care.
      # The check for non-regional service types is included, because the rule might get evaluated before the count.
      condition     = lower(var.backend_service_type) != "regional" || (lower(var.backend_service_type) == "regional" && (!var.create_backend_service || (var.node_count % 3 == 0)))
      error_message = "When `backend_service_type` is `regional` and `create_backend_service` is true, `node_count` must be divisible by 3."
    }
  }
}

resource "google_compute_region_backend_service" "additional_nics" {
  project = var.project
  count   = var.backend_service_type == "regional" && var.create_backend_service ? length(var.additional_nics) : 0

  name     = format("%v-%v-regional", var.additional_nics[count.index]["network_name"], var.name)
  protocol = "TCP"
  network  = var.additional_nics[count.index]["network_name"]

  backend {
    group = google_compute_instance_group.default[0].self_link
  }

  backend {
    group = google_compute_instance_group.default[1].self_link
  }

  backend {
    group = google_compute_instance_group.default[2].self_link
  }

  health_checks = local.health_checks
}

resource "google_compute_backend_service" "default" {
  project = var.project
  count   = var.backend_service_type != "regional" && !var.enable_iap && var.create_backend_service ? 1 : 0

  name        = format("%v-%v", var.environment, var.name)
  protocol    = var.backend_protocol
  port_name   = var.name
  timeout_sec = var.timeout_sec

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[2].self_link
  }

  connection_draining_timeout_sec = var.backend_connection_draining_timeout

  health_checks = local.health_checks
}

resource "google_compute_backend_service" "iap" {
  project = var.project
  count   = var.backend_service_type != "regional" && var.enable_iap && var.create_backend_service ? 1 : 0

  name        = format("%v-%v", var.environment, var.name)
  protocol    = var.backend_protocol
  port_name   = var.name
  timeout_sec = var.timeout_sec

  backend {
    group = google_compute_instance_group.default[0].self_link
  }

  backend {
    group = google_compute_instance_group.default[1].self_link
  }

  backend {
    group = google_compute_instance_group.default[2].self_link
  }

  health_checks = local.health_checks

  iap {
    enabled              = true
    oauth2_client_secret = var.oauth2_client_secret
    oauth2_client_id     = var.oauth2_client_id
  }
}

resource "google_compute_health_check" "tcp" {
  project = var.project
  count   = var.create_backend_service && var.health_check == "tcp" ? 1 : 0

  name = format("%v-%v-tcp", var.environment, var.name)

  tcp_health_check {
    port = var.health_check_port == 0 ? var.service_port : var.health_check_port
  }
}

resource "google_compute_health_check" "http" {
  project = var.project
  count   = var.create_backend_service && var.health_check == "http" ? 1 : 0

  name = format("%v-%v-http", var.environment, var.name)

  http_health_check {
    port         = var.health_check_port == 0 ? var.service_port : var.health_check_port
    request_path = var.service_path
  }
}

resource "google_compute_instance_group" "default" {
  count = length(local.zones)

  project     = var.project
  name        = format("%v-%v-%v", var.environment, var.name, local.zones[count.index])
  description = "Instance group for monitoring VM."
  zone        = local.zones[count.index]

  named_port {
    name = "http"
    port = "80"
  }

  named_port {
    name = "https"
    port = "443"
  }

  named_port {
    name = var.name
    port = var.service_port
  }

  # This filters the full set of instances to only ones for the appropriate zone.
  instances = concat(
    matchkeys(
      google_compute_instance.default[*].self_link,
      google_compute_instance.default[*].zone,
      [local.zones[count.index]],
    ),
    [
      for key, node in local.merged_nodes :
      google_compute_instance.instance_zoned[key].self_link
      if google_compute_instance.instance_zoned[key].zone == local.zones[count.index]
    ]
  )
}
