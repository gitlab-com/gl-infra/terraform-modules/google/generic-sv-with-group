output "instances" {
  value = concat(
    coalesce(google_compute_instance.default[*], []),
    coalesce(
      [
        for key, node in coalesce(local.merged_nodes, {}) :
        google_compute_instance.instance_zoned[key]
      ],
      []
    )
  )
}

output "instances_self_link" {
  value = concat(
    coalesce(google_compute_instance.default[*].self_link, []),
    coalesce(
      [
        for key, node in coalesce(local.merged_nodes, {}) :
        google_compute_instance.instance_zoned[key].self_link
      ],
      []
    )
  )
}

output "instance_groups_self_link" {
  value = google_compute_instance_group.default[*].self_link
}

output "tcp_health_check_self_link" {
  value = var.create_backend_service && var.health_check == "tcp" ? google_compute_health_check.tcp[0].self_link : ""
}

output "http_health_check_self_link" {
  value = var.create_backend_service && var.health_check == "http" ? google_compute_health_check.http[0].self_link : ""
}

# This is idiomatic for how to have outputs that may have a count of zero
# https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
output "google_compute_backend_service_self_link" {
  value = element(
    concat(google_compute_backend_service.default[*].self_link, [""]),
    0,
  )
}

output "google_compute_backend_service_iap_self_link" {
  value = element(
    concat(google_compute_backend_service.iap[*].self_link, [""]),
    0,
  )
}

output "google_compute_region_backend_service_self_link" {
  value = element(
    concat(
      google_compute_region_backend_service.default[*].self_link,
      [""],
    ),
    0,
  )
}

output "google_compute_region_backend_service_additional_nics_self_link" {
  value = var.create_backend_service ? zipmap(
    [for nic in var.additional_nics : nic["network_name"]],
    google_compute_region_backend_service.additional_nics[*].self_link
  ) : {}
}

output "google_compute_subnetwork_name" {
  value = element(concat(google_compute_subnetwork.subnetwork[*].name, [""]), 0)
}

output "google_compute_subnetwork_self_link" {
  value = element(
    concat(google_compute_subnetwork.subnetwork[*].self_link, [""]),
    0,
  )
}

output "google_compute_additional_subnetworks_self_link" {
  value = zipmap(
    [for nic in var.additional_nics : nic["network_name"]],
    google_compute_subnetwork.additional_subnetwork[*].self_link
  )
}

output "google_compute_address_static_ips" {
  value = element(
    concat(google_compute_address.static-ip-address[*].address, [""]),
    0,
  )
}
