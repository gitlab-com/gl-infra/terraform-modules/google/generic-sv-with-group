variable "health_check_port" {
  type        = number
  description = "Health check port, if 0 use `service_port` (default)"
  default     = 0
}

variable "use_external_ip" {
  type    = bool
  default = false
}

variable "static_private_ip" {
  type        = bool
  default     = false
  description = "Create Google compute instances with static internal IP addresses"
}

variable "static_private_ip_auto_assign" {
  type        = bool
  default     = false
  description = "Let the static internal IP addresses be assigned automatically instead of assigning them explicitly"
}

variable "backend_service_type" {
  type        = string
  default     = "regular"
  description = "Type of backend service, either normal or regional"
}

variable "backend_balancing_mode" {
  type        = string
  description = "Balancing mode of the backend service: `UTILIZATION`, `RATE` or `CONNECTION`"
  default     = "UTILIZATION"
}

variable "backend_connection_draining_timeout" {
  type        = number
  description = "Time (in seconds) for which instance will be drained from the backend service (not accept new connections, but still work to finish started)"
  default     = 300
}

variable "kernel_version" {
  type    = string
  default = ""
}

variable "use_new_node_name" {
  type    = bool
  default = false
}

variable "default_scopes" {
  type = list(string)

  default = [
    "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/pubsub",
    "https://www.googleapis.com/auth/service.management.readonly",
    "https://www.googleapis.com/auth/servicecontrol",
    "https://www.googleapis.com/auth/trace.append",
    "https://www.googleapis.com/auth/cloudkms",
    "https://www.googleapis.com/auth/compute.readonly",
  ]

  description = "default permission scopes."
}

variable "additional_scopes" {
  type        = list(string)
  default     = []
  description = "Additional permission scopes."
}

variable "create_backend_service" {
  type    = bool
  default = true
}

variable "egress_ports" {
  type        = list(string)
  description = "The list of ports that should be opened for egress traffic"
  default     = []
}

variable "enable_iap" {
  type    = bool
  default = false
}

variable "oauth2_client_id" {
  type    = string
  default = ""
}

variable "oauth2_client_secret" {
  type    = string
  default = ""
}

variable "allow_stopping_for_update" {
  type        = bool
  description = "Whether Terraform is allowed to stop the instance to update its properties"
  default     = false
}

variable "backend_protocol" {
  type    = string
  default = "HTTP"
}

variable "health_check" {
  type    = string
  default = "http"

  validation {
    condition     = contains(["http", "tcp"], var.health_check)
    error_message = "The health_check value must be \"http\" or \"tcp\"."
  }
}

variable "service_port" {
  type    = number
  default = 80
}

variable "service_path" {
  type    = string
  default = "/"
}

variable "timeout_sec" {
  type    = number
  default = 30
}

variable "subnetwork_name" {
  type        = string
  default     = ""
  description = "Subnetwork name for the instances"
}

variable "block_project_ssh_keys" {
  type        = bool
  description = "Whether to block project level SSH keys"
  default     = true
}

variable "chef_init_run_list" {
  type        = string
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "enable_oslogin" {
  type        = bool
  description = "Whether to enable OS Login GCP feature"
  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = false
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range"
  default     = "169.254.0.1/32"
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "node_count" {
  type        = number
  description = "The nodes count"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = bool
  description = "Use preemptible instances for this pet"
  default     = false
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "bootstrap_script" {
  type        = string
  description = "User-provided bootstrap script to override the bootstrap module"
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "User-provided teardown script to override the bootstrap module"
  default     = null
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "zone" {
  type        = string
  description = "DEPRECATED, use zones instead. Create instances in the specified zone instead of all region zones"
  default     = ""
}

variable "zones" {
  type        = list(string)
  description = "List of zones to create Instances in, defaults to all region zones if left empty"
  default     = []
}

variable "deletion_protection" {
  type    = bool
  default = false
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

# Example value is as follows:
# [{
#   "network_name"  = "foo-vpc"
#   "ip_cidr_range" = "10.10.0.0/20"
# }]
variable "additional_nics" {
  type    = list(map(string))
  default = []
}

# Additional instance tags
variable "additional_tags" {
  type    = list(string)
  default = []
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}

# OS disk snapshot variables
variable "enable_os_disk_snapshots" {
  type        = bool
  description = "Enable creation of OS disk snapshot resource and attachment policies"
  default     = false
}

variable "os_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 3."
  default     = 3
}

variable "enable_os_snapshot_guest_scripts" {
  type        = bool
  description = "Enables application-consistent snapshots so that pre and post snap scripts are executed"
  default     = true
}

variable "hours_between_os_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly os disk snapshots, 0 to disable"
  default     = 4
}

variable "os_disk_snapshot_nodes" {
  type        = map(string)
  description = "Map of node index => snapshot self_link to use when creating new nodes from an OS disk snapshot instead of existing boot image"
  default     = {}
}

variable "nodes_count" {
  type        = number
  description = "Number of nodes to provision using the 'nodes' map in a for_each loop."
  default     = 0
}

variable "nodes_offset" {
  type        = number
  description = "The starting index for provisioning nodes with the `nodes` map or `nodes_count` variables. Useful when migrating from provisioning using count to for_each"
  default     = 0
}

variable "balance_across_zones" {
  type    = bool
  default = false
}

variable "nodes" {
  type = map(object({
    chef_run_list_extra    = optional(string, "")
    zone                   = optional(string, "")
    balance_across_zones   = optional(bool, false)
    machine_type           = optional(string, "")
    os_disk_type           = optional(string, "")
    log_disk_type          = optional(string, "")
    os_disk_snapshot       = optional(string, "")
    os_boot_image_override = optional(string, "")
    additional_labels      = optional(map(string), {})
    labels_override        = optional(map(string), {})
  }))

  default = {}

  validation {
    condition = alltrue([
      for id, node in var.nodes : can(regex("^\\d+$", id))
    ])
    error_message = "All nodes must have a numeric key."
  }
}

variable "nodes_overrides" {
  type = map(object({
    chef_run_list_extra    = optional(string, "")
    zone                   = optional(string, "")
    balance_across_zones   = optional(bool, false)
    machine_type           = optional(string, "")
    os_disk_type           = optional(string, "")
    log_disk_type          = optional(string, "")
    os_disk_snapshot       = optional(string, "")
    os_boot_image_override = optional(string, "")
    additional_labels      = optional(map(string), {})
    labels_override        = optional(map(string), {})
  }))

  default = {}

  description = "Node parameters to override for a specific node index that is provisioned using `nodes` or `nodes_count`."

  validation {
    condition = alltrue([
      for id, node in var.nodes_overrides : can(regex("^\\d+$", id))
    ])
    error_message = "All nodes must have a numeric key."
  }
}
