resource "google_compute_address" "static-ip-address_zoned" {
  for_each = var.static_private_ip ? local.merged_nodes : {}

  name = format("%v-%02d-%v-%v-static-ip",
    var.name,
    100 + each.key,
    var.tier,
    var.environment,
  )

  address_type = "INTERNAL"
  address      = var.static_private_ip_auto_assign ? null : cidrhost(var.ip_cidr_range, each.key + 100)
  subnetwork   = var.subnetwork_name == "" ? google_compute_subnetwork.subnetwork[0].self_link : data.google_compute_subnetwork.subnetwork[0].self_link
  region       = var.region
}

resource "google_compute_disk" "log_disk_zoned" {
  for_each = local.merged_nodes

  name = format("%v-%02d-%v-%v-log",
    var.name,
    each.key,
    var.tier,
    var.environment,
  )
  project = var.project
  zone    = coalesce(each.value.zone, each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)

  size = var.log_disk_size
  type = coalesce(each.value.log_disk_type, var.log_disk_type)

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_disk" "os_disk_from_snapshot_zoned" {
  for_each = local.os_disk_create_nodes

  project  = var.project
  zone     = coalesce(each.value.zone, each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)
  snapshot = each.value.os_disk_snapshot
  name     = format("%v-%02d-%v-%v", var.name, each.key, var.tier, var.environment)
  labels   = var.labels
  type     = coalesce(each.value.os_disk_type, var.os_disk_type)
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_instance" "instance_zoned" {
  for_each = local.merged_nodes

  name                      = format("%v-%02d-%v-%v", var.name, each.key, var.tier, var.environment)
  project                   = var.project
  zone                      = coalesce(each.value.zone, each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)
  machine_type              = coalesce(each.value.machine_type, var.machine_type)
  allow_stopping_for_update = var.allow_stopping_for_update

  metadata = {
    "CHEF_URL"         = var.chef_provision["server_url"]
    "CHEF_VERSION"     = var.chef_provision["version"]
    "CHEF_ENVIRONMENT" = var.environment
    "CHEF_NODE_NAME" = var.use_new_node_name ? format("%v-%02d-%v-%v.c.%v.internal",
      var.name,
      each.key,
      var.tier,
      var.environment,
      var.project,
      ) : format("%v-%02d.%v.%v.%v",
      var.name,
      each.key,
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "GL_KERNEL_VERSION" = var.kernel_version
    "CHEF_RUN_LIST" = join(",", compact(
      [
        var.chef_run_list,
        each.value.chef_run_list_extra
      ]
    ))
    "CHEF_DNS_ZONE_NAME"     = var.dns_zone_name
    "CHEF_PROJECT"           = var.project
    "CHEF_BOOTSTRAP_BUCKET"  = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING" = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"     = var.chef_provision["bootstrap_key"]
    "CHEF_INIT_RUN_LIST"     = var.chef_init_run_list
    "block-project-ssh-keys" = upper(tostring(var.block_project_ssh_keys))
    "enable-oslogin"         = upper(tostring(var.enable_oslogin))
    "shutdown-script"        = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"         = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = each.value.os_disk_snapshot != "" ? google_compute_disk.os_disk_from_snapshot_zoned[each.key].self_link : null
    dynamic "initialize_params" {
      for_each = each.value.os_disk_snapshot == "" ? [0] : []
      content {
        image  = each.value.os_boot_image_override != "" ? each.value.os_boot_image_override : var.os_boot_image
        size   = var.os_disk_size
        type   = coalesce(each.value.os_disk_type, var.os_disk_type)
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.log_disk_zoned[each.key].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = var.subnetwork_name == "" ? google_compute_subnetwork.subnetwork[0].self_link : data.google_compute_subnetwork.subnetwork[0].self_link
    network_ip = var.static_private_ip ? google_compute_address.static-ip-address_zoned[each.key].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, var.node_count + index(local.merged_nodes_list, each.key)) : ""
      }
    }
  }

  dynamic "network_interface" {
    for_each = var.additional_nics
    content {
      network            = network_interface.value["network_name"]
      subnetwork         = google_compute_subnetwork.additional_subnetwork[network_interface.key].self_link
      subnetwork_project = var.project
    }
  }

  labels = length(each.value.labels_override) > 0 ? each.value.labels_override : merge(var.labels, each.value.additional_labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [
      boot_disk[0].initialize_params[0].labels,
      boot_disk[0].initialize_params[0].size,
      boot_disk[0].initialize_params[0].type
    ]
    precondition {
      error_message = "Keys in the nodes map must not intersect with indices supplied via the node_count variable."
      condition     = length(setintersection([for k in keys(var.nodes) : format("%02d", k)], [for i in range(var.node_count) : format("%02d", i + 1)])) == 0
    }
  }
}
