locals {
  zones = var.zone != "" ? [var.zone] : (length(var.zones) > 0 ? var.zones : data.google_compute_zones.available.names)

  external_ips  = concat(google_compute_address.external[*].address, [""])
  health_checks = concat(google_compute_health_check.http[*].self_link, google_compute_health_check.tcp[*].self_link)

  os_disk_create_nodes = {
    for index, node in var.nodes : index => node if node.os_disk_snapshot != ""
  }

  nodes_from_count = {
    for idx in range(0 + var.nodes_offset, var.nodes_offset + var.nodes_count) :
    format("%02d", idx + 1) => {
      zone                   = local.zones[idx % length(local.zones)]
      balance_across_zones   = var.balance_across_zones
      machine_type           = var.machine_type
      os_disk_type           = var.os_disk_type
      log_disk_type          = var.log_disk_type
      labels_override        = {}
      additional_labels      = {}
      os_disk_snapshot       = ""
      os_boot_image_override = ""
      chef_run_list_extra    = ""
    }
  }

  merged_nodes = merge(var.nodes_count > 0 ? local.nodes_from_count : var.nodes, var.nodes_overrides)

  merged_nodes_list = [
    for key, node in local.merged_nodes : key
  ]
}
