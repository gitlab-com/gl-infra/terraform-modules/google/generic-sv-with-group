resource "google_compute_subnetwork" "subnetwork" {
  count                    = var.subnetwork_name == "" ? 1 : 0
  name                     = format("%v-%v", var.name, var.environment)
  network                  = var.vpc
  project                  = var.project
  region                   = var.region
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "additional_subnetwork" {
  count                    = length(var.additional_nics)
  name                     = format("%v-%v", var.name, var.additional_nics[count.index]["network_name"])
  network                  = var.additional_nics[count.index]["network_name"]
  project                  = var.project
  region                   = var.region
  ip_cidr_range            = var.additional_nics[count.index]["ip_cidr_range"]
  private_ip_google_access = true
}
