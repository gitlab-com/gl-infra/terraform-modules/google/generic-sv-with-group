resource "google_compute_address" "static-ip-address" {
  project = var.project
  count   = var.static_private_ip ? var.node_count : 0

  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1 + 100,
    var.tier,
    var.environment,
  )
  address_type = "INTERNAL"

  address    = var.static_private_ip_auto_assign ? null : cidrhost(var.ip_cidr_range, count.index + 1 + 100)
  subnetwork = var.subnetwork_name == "" ? google_compute_subnetwork.subnetwork[0].self_link : data.google_compute_subnetwork.subnetwork[0].self_link

  lifecycle {
    ignore_changes = [description]
  }
  region = var.region

}

# Add one instance group per zone and only select the appropriate instances for each one

resource "google_compute_disk" "log_disk" {
  project = var.project
  count   = var.node_count

  name = format(
    "%v-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = element(local.zones, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_disk" "os_disk_from_snapshot" {
  for_each = { for idx, snap in var.os_disk_snapshot_nodes : idx => snap if snap != "" }

  project = var.project
  name = format("%v-%02d-%v-%v", var.name, each.key, var.tier,
    var.environment,
  )
  snapshot = each.value
  labels   = var.labels
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_instance" "default" {
  count = var.node_count

  project                   = var.project
  name                      = format("%v-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)
  machine_type              = var.machine_type
  deletion_protection       = var.deletion_protection
  allow_stopping_for_update = var.allow_stopping_for_update

  // The index is wrapped around by taking the index modulo the length of the list.
  // This effectively distribute the instances over the target zones.
  zone = element(local.zones, count.index + 1)

  metadata = {
    "CHEF_URL"     = var.chef_provision["server_url"]
    "CHEF_VERSION" = var.chef_provision["version"]
    "CHEF_NODE_NAME" = var.use_new_node_name ? format("%v-%02d-%v-%v.c.%v.internal",
      var.name, count.index + 1, var.tier, var.environment, var.project,
      ) : format("%v-%02d.%v.%v.%v",
      var.name, count.index + 1, var.tier, var.environment, var.dns_zone_name,
    )
    "GL_KERNEL_VERSION"      = var.kernel_version
    "CHEF_ENVIRONMENT"       = var.environment
    "CHEF_INIT_RUN_LIST"     = var.chef_init_run_list
    "CHEF_RUN_LIST"          = var.chef_run_list
    "CHEF_DNS_ZONE_NAME"     = var.dns_zone_name
    "CHEF_PROJECT"           = lookup(var.chef_provision, "bootstrap_gcp_project", var.project)
    "CHEF_BOOTSTRAP_BUCKET"  = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING" = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"     = var.chef_provision["bootstrap_key"]
    "block-project-ssh-keys" = upper(tostring(var.block_project_ssh_keys))
    "enable-oslogin"         = upper(tostring(var.enable_oslogin))
    "shutdown-script"        = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"         = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = concat(var.default_scopes, var.additional_scopes)
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = lookup(var.os_disk_snapshot_nodes, tostring(count.index), "") != "" ? google_compute_disk.os_disk_from_snapshot[tostring(count.index)].self_link : null
    dynamic "initialize_params" {
      for_each = lookup(var.os_disk_snapshot_nodes, tostring(count.index), "") != "" ? [] : [0]
      content {
        image = var.os_boot_image
        size  = var.os_disk_size
        type  = var.os_disk_type

        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork         = var.subnetwork_name != "" ? var.subnetwork_name : one(google_compute_subnetwork.subnetwork[*].name)
    subnetwork_project = var.project
    network_ip         = var.static_private_ip ? google_compute_address.static-ip-address[count.index].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index) : ""
      }
    }
  }

  dynamic "network_interface" {
    for_each = var.additional_nics
    content {
      network            = network_interface.value["network_name"]
      subnetwork         = google_compute_subnetwork.additional_subnetwork[network_interface.key].self_link
      subnetwork_project = var.project
    }
  }

  tags = setunion(
    [
      var.name,
      var.environment,
    ],
    var.additional_tags
  )

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
    service     = var.name
  })

  lifecycle {
    ignore_changes = [
      boot_disk[0].initialize_params[0].labels,
      boot_disk[0].initialize_params[0].size,
      boot_disk[0].initialize_params[0].type
    ]
  }
}
